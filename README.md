# Keynes example

Purpose of this repository is to show how to use
[Keynes](https://gitlab.com/sbitio/keynes) library.

This repository contains:

 * Example of all Keynes features
 * Instructions to set up Jenkins with JobDSL and the Keynes library
 * Dockerfile and CasC config to build a Jenkins image ready to run a
 Keynes-based project
 * A script to export all job definitions to a folder (see `tools/export-jobs`)

# Setup

## Manual setup of Jenkins to run a Keynes-based project

### 0. Install jenkins

...

### 1. Install dependencies

Install the plugins listed in the Keynes README or [docker/plugins.txt](./docker/plugins.txt)

### 2. Declare global library

Go to `Jenkins > Manage Jenkins > Configure System > Global Pipeline Libraries`
and fill at least:

```
Name: keynes
Project Repository: https://gitlab.com/sbitio/keynes
```

### 3. Create the seed job

Go to `Jenkins > New item`. Create a job of type `Pipeline` and fill at least:

```
Definition: Pipeline script from SCM
SCM: Git
Repository URL: ...
```

### 4. Allow JobDSL to run

Default security policy forbids execution of JobDSL code. You want
to install and configure [Authorize Project](https://plugins.jenkins.io/authorize-project)
plugin.

Alternatively you can disable JobDSL security, at your own risk.

### 5. Run it

Run the seed job.

## Docker

`docker/` subfolder contains a Dockerfile based on [jenkins/jenkins:lts](https://hub.docker.com/r/jenkins/jenkins)
that sets up Jenkins to run a Keynes-based project by implementing
the above instructions. For convenience it also creates user `keynes`
with pass `keynes`.

Note on security: for simplicity we opted to disable JobDSL security,
so we don't recomend to use this image in a production environment
(patches welcome).

The Dockerfile installs a seed job named `_seed`, linked to `/seed` in
the container, so you want to mount a local folder with your projects on
that location.

### Instructions

### Build the image

```bash
docker pull jenkins/jenkins:lts
docker build -t keynes ./docker
```

### Run the container

```bash
NAME=keynes-example
docker run --name $NAME \
-p 8080:8080 \
--volume $NAME:/var/jenkins_home \
--mount type=bind,source="$(pwd)",target=/seed \
keynes
```

### Run the container for Keynes development

To ease development of Keynes without the need to push every change
to gitlab.com, a local copy of Keynes can be exposed to the docker image
and loaded with `filesystem_scm` plugin.

For this, create the container as this:

```bash
PATH_TO_KEYNES=/opt/sbitio/keynes
NAME=keynes-example
docker run --name $NAME \
-p 8080:8080 \
--volume $NAME:/var/jenkins_home \
--mount type=bind,source="$(pwd)",target=/seed \
--mount type=bind,source="$PATH_TO_KEYNES",target=/keynes \
keynes
```

Then go to /configure section "Global Pipeline Libraries" and change the
library SCM to: Legacy SCM, File System, Path = /keynes.

Don't forget to save/apply the changes.

From this point on, any change to the files in the Keynes clone are available
in Jenkins with no need to commit + push.

### Cleanup

```bash
docker container stop $NAME
docker container rm $NAME
docker volume rm $NAME
docker image rm keynes
```

# Development

Development happens on [Gitlab](https://gitlab.com/sbitio/keynes).

Please log issues for any bug report, feature or support request.

# License

MIT License, see LICENSE file

# Contact

Please use the contact form on http://sbit.io

